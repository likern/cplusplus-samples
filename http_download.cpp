#include <boost/filesystem.hpp>
#include "http_download.hpp"

using namespace std;
using namespace boost::filesystem;

HTTPDownload::HTTPDownload(std::string &source) : CurlEasy()
{
    if (source.find("http://") != 0)
    {
        cout << "HTTP source: " << source << "is not valid" << endl;
        throw std::runtime_error("Is not valid HTTP address");
    }

    CURLcode status = CURLE_OK;
    status = curl_easy_setopt(handle,  CURLOPT_URL, source.c_str());
    if (status != CURLE_OK)
    {
        cout << "Can't set URL:" << source << endl;
        throw std::runtime_error("Can't set URL option");
    }
    status = curl_easy_setopt(handle,  CURLOPT_WRITEFUNCTION, writeToFile);
    if (status != CURLE_OK)
    {
        cout << "Can't set WRITEFUNCTION parameter" << endl;
        throw std::runtime_error("Can't set WRITEFUNCTION option");
    }

    status = curl_easy_setopt(handle,  CURLOPT_WRITEDATA , &file);
    if (status != CURLE_OK)
    {
        cout << "Can't set WRITEFUNCTION parameter" << endl;
        throw std::runtime_error("Can't set WRITEFUNCTION option");
    }

    this->source = source;
}

void HTTPDownload::saveToFile(path &p)
{
    file.open(p, ios::trunc);
    if (file.fail())
    {
        cout << "can't open desired file:" << p << endl;
        throw std::runtime_error("Can't open file");
    }

    CURLcode status;
    status = curl_easy_perform(handle);
    if (status != CURLE_OK)
    {
        cout << "can't open desired file:" << p
        << curl_easy_strerror(status) << endl;
        throw std::runtime_error("Can't download file");
    }

    file.close();
}

HTTPDownload::~HTTPDownload()
{
    //if (file.is_iopen())
    /*if (file.is_iopen())
    {
        file.close();
    }*/
}

void *CreateHTTPPlugin(std::string &source)
{
    return new HTTPDownload(source);
}

void DeleteHTTPPlugin(void *plug)
{
    HTTPDownload *plugin;
    //PluginObjectInterface *plugin;
    plugin = static_cast<HTTPDownload *> (plug);
    delete plugin;
}

/*pluginGetInfo()
{
    PluginInfo info;
    info.minor = 0;
    info.major = 1;
    info.type = HTTP;
    return info;
}*/

PluginInterface InitPlugin()
{
    PluginInterface interface;
    PluginInfo info = {0, 1, HTTP}; 
    interface.info = info;
    interface.create = CreateHTTPPlugin;
    interface.destroy = DeleteHTTPPlugin;
    return interface;
}