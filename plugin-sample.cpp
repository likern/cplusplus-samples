#include <cstdlib>
#include "plugin.hpp"

using namespace boost::filesystem;

#define FTP_FILE "ftp://debian.nsu.ru/debian-cd/7.4.0/i386/list-dvd/debian-7.4.0-i386-DVD-1.list.gz"
#define HTTP_FILE "http://debian.nsu.ru/debian-cd/project/build/3.1_r0/i386"

int main(int argc, char** argv)
{
    PluginSystem &plugSystem = PluginSystem::getInstance();
    path currPath = current_path();
    plugSystem.loadAll(currPath);

    std::list<PluginInterface> plugins = plugSystem.getPlugins();

    path templatePath("file.download.%%%%%");

    std::list<PluginInterface>::const_iterator it(plugins.begin());
    for(; it != plugins.end(); ++it)
    {
        PluginObjectInterface *obj;

        if ((it->info).type == FTP)
        {
            std::cout << "FTP plugin" << std::endl;
            std::string ftp(FTP_FILE);
            void *tmpObject = (it->create) (ftp);
            obj = static_cast<PluginObjectInterface *> (tmpObject);
        }
        else if ((it->info).type == HTTP)
        {
            std::cout << "HTTP plugin" << std::endl;
            std::string http(HTTP_FILE);
            void *tmpObject = (it->create) (http);
            obj = static_cast<PluginObjectInterface *> (tmpObject);
        } 
        else
        {
            std::cout << "Unknow plugin TYPE" << std::endl;
            continue;
        }

        path realPath = unique_path(templatePath);
        obj->saveToFile(realPath);
        std::cout << "Save to file:" << realPath << std::endl;
    }
    plugSystem.unloadAll();
}