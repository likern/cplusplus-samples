#include <boost/filesystem.hpp>
#include "ftp_download.hpp"

using namespace std;
using namespace boost::filesystem;

FTPDownload::FTPDownload(std::string &source) : CurlEasy()
{
    if (source.find("ftp://") != 0)
    {
        cout << "FTP source: " << source << "is not valid" << endl;
        throw std::runtime_error("Is not valid FTP address");
    }

    CURLcode status = CURLE_OK;
    status = curl_easy_setopt(handle,  CURLOPT_URL, source.c_str());
    if (status != CURLE_OK)
    {
        cout << "Can't set URL:" << source << endl;
        throw std::runtime_error("Can't set URL option");
    }
    status = curl_easy_setopt(handle,  CURLOPT_WRITEFUNCTION, writeToFile);
    if (status != CURLE_OK)
    {
        cout << "Can't set WRITEFUNCTION parameter" << endl;
        throw std::runtime_error("Can't set WRITEFUNCTION option");
    }
    //status = curl_easy_setopt(handle,  CURLOPT_WRITEDATA , NULL);
    status = curl_easy_setopt(handle,  CURLOPT_WRITEDATA , &file);
    if (status != CURLE_OK)
    {
        cout << "Can't set WRITEFUNCTION parameter" << endl;
        throw std::runtime_error("Can't set WRITEFUNCTION option");
    }

    this->source = source;
}

void FTPDownload::saveToFile(path &p)
{
    file.open(p, ios::trunc);
    if (file.fail())
    {
        cout << "can't open desired file:" << p << endl;
        throw std::runtime_error("Can't open file");
    }

    CURLcode status;
    status = curl_easy_perform(handle);
    if (status != CURLE_OK)
    {
        cout << "can't open desired file:" << p
        << curl_easy_strerror(status) << endl;
        throw std::runtime_error("Can't download file");
    }

    file.close();
}

FTPDownload::~FTPDownload()
{
    //if (file.is_iopen())
    /*if (file.is_iopen())
    {
        file.close();
    }*/
}

void *CreateFTPPlugin(std::string &source)
{
    return new FTPDownload(source);
}

void DeleteFTPPlugin(void *plug)
{
    PluginObjectInterface *plugin;
    plugin = static_cast<PluginObjectInterface *> (plug);
    delete plugin;
}

/*pluginGetInfo()
{
    PluginInfo info;
    info.minor = 0;
    info.major = 1;
    info.type = FTP;
    return info;
}*/

PluginInterface InitPlugin()
{
    PluginInterface interface;
    PluginInfo info = {0, 1, FTP}; 
    interface.info = info;
    interface.create = CreateFTPPlugin;
    interface.destroy = DeleteFTPPlugin;
    return interface;
}