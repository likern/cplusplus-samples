#ifndef _FTP_DOWNLOAD_HPP_
#define _FTP_DOWNLOAD_HPP_

#include <string>
#include <fstream>
#include <boost/filesystem/fstream.hpp>
#include "plugin_interface.hpp"
#include "download.hpp"

class FTPDownload : private PluginObjectInterface, protected CurlEasy
{
    public:
        FTPDownload(std::string &source);
        ~FTPDownload();
        void saveToFile(boost::filesystem::path &p);
    private:
        FTPDownload(const FTPDownload &file);
        FTPDownload & operator=(const FTPDownload&);
        std::string source;
        boost::filesystem::path dest;
        boost::filesystem::ofstream file;
};

#endif