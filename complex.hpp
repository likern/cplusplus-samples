#ifndef _COMPLEX_HPP_
#define _COMPLEX_HPP_

#include <iostream>
#include <ostream>

class Complex {
    double real, imag;  

public:
    Complex(double real = 0, double imag = 0);
    Complex operator+(Complex &val) const;
    Complex operator-(Complex &val) const;
    Complex operator*(Complex &val) const;
    Complex operator*(double val) const;
    Complex operator/(Complex &val) const;
    //ostream &operator<<(ostream &, Complex &val);
    double module() const;
    double getReal() const;
    double getImag() const;
};

#endif