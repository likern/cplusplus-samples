#include <ios>
#include <iostream>
#include <fstream>
#include "download.hpp"

CurlEasy::CurlEasy()
{
    if (count == 0)
    {
        if (curl_global_init(CURL_GLOBAL_ALL) != CURLE_OK)
        {
            throw std::runtime_error("Can't initialize curl library");
        }
    }
    ++count;

    handle = curl_easy_init();
    if (handle == NULL)
    {
        --count;
        if (!count)
        {
            curl_global_cleanup();
        }
        throw std::runtime_error("Can't initialize curl easy object");
    }
}

CurlEasy::~CurlEasy()
{
    curl_easy_cleanup(handle);
    --count;
    if (!count)
    {
        curl_global_cleanup();
    }
}

size_t writeToFile(char *ptr, size_t size, size_t nmemb, void *usrdata)
{
    size_t bytes = size * nmemb;
    if (!ptr || !usrdata)
    {
        return !bytes;
    }

    std::ofstream *file = static_cast<std::ofstream *> (usrdata);
    file->write(ptr, bytes);
    if(file->fail())
    {
        std::cout << "can't write data to file: " << *file << std::endl;
        return !bytes;
    }
    return bytes;
}