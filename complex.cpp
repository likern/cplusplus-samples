#include <cmath>
#include <stdexcept>
#include "complex.hpp"

Complex::Complex(double rl, double img)
{
    real = rl;
    imag = img;
}

double Complex::getReal() const
{
    return real;
}

double Complex::getImag() const
{
    return imag;
}

Complex Complex::operator+(Complex &val) const
{
    double tmp_real, tmp_imag;
    tmp_real = real + val.real;
    tmp_imag = imag + val.imag;
    Complex tmp(tmp_real, tmp_imag);
    return tmp;
}

Complex Complex::operator-(Complex &val) const
{
    double tmp_real, tmp_imag;
    tmp_real = (real - val.real);
    tmp_imag = (imag - val.imag);
    Complex tmp(tmp_real, tmp_imag);
    return tmp;
}

Complex Complex::operator*(Complex &val) const
{
    double tmp_real, tmp_imag;
    tmp_real = (real * val.real - imag * val.imag);
    tmp_imag = (real * val.imag + imag * val.real);
    Complex tmp(tmp_real, tmp_imag);
    return tmp;
}

Complex Complex::operator*(double val) const
{
    Complex tmp(val * real, val * imag);
    return tmp;
}

Complex Complex::operator/(Complex &val) const
{
    double tmp_real, tmp_imag, denom;
    denom = std::pow(val.real, 2.0) + std::pow(val.imag, 2.0);
    if (!denom)
    {
        throw std::overflow_error("Divide by zero");
    }
    tmp_real = (real * val.real + imag * val.imag) / denom;
    tmp_imag = (imag * val.real - real * val.imag) / denom;
    Complex tmp(tmp_real, tmp_imag);
    return tmp;
}

double Complex::module() const
{
    double pw = std::pow(real, 2) + std::pow(imag, 2);
    double tmp = std::sqrt(pw);
    return tmp;
}

/*ostream& operator<<(ostream&, Complex &val)
{
    ostream << "Complex(" << val.getReal() << ", ";
    ostream << val.getImag() << ")"<< endl;
    return ostream;
}*/