#ifndef _HTTP_DOWNLOAD_HPP_
#define _HTTP_DOWNLOAD_HPP_

#include <string>
#include <fstream>
#include <boost/filesystem/fstream.hpp>

#include "download.hpp"
#include "plugin_interface.hpp"

class HTTPDownload : private PluginObjectInterface, protected CurlEasy
{
    public:
        HTTPDownload(std::string &source);
        ~HTTPDownload();
        void saveToFile(boost::filesystem::path &p);
    private:
        HTTPDownload(const HTTPDownload &file);
        HTTPDownload & operator=(const HTTPDownload&);
        std::string source;
        boost::filesystem::path dest;
        boost::filesystem::ofstream file;
};

#endif