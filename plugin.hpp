#ifndef _PLUGIN_HPP_
#define _PLUGIN_HPP_

#include <boost/filesystem.hpp>
#include "plugin_interface.hpp"

struct SharedLibrary
{
    void *handle;
};

class PluginSystem
{
    public:
        static PluginSystem& getInstance();
        void loadAll(boost::filesystem::path &directory);
        void unloadAll();
        std::list<PluginInterface> getPlugins();
    private:
        PluginSystem() {};
        PluginSystem(const PluginSystem&) {};
        PluginSystem& operator=(const PluginSystem&) {};
        std::list<SharedLibrary> libs;
        std::list<PluginInterface> plugins;
};

#endif