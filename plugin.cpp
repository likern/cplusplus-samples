#include <dlfcn.h>
#include "plugin.hpp"

using namespace boost::filesystem;

void PluginSystem::loadAll(path &p)
{
    if (!exists(p))
    {
        std::cout << "Path doesn't exist" << std::endl;
        throw std::runtime_error("Path doesn't exist");
    }

    if (!is_directory(p))
    {
        std::cout << "Not a directory" << std::endl;
        throw std::runtime_error("Not a directory");
    }

    std::vector<path> vec;
    std::copy(directory_iterator(p), directory_iterator(), back_inserter(vec));
    std::vector<path>::const_iterator it(vec.begin());

    // Iterate over all files and loads all shared libs
    for (; it != vec.end() ; ++it)
    {
        path p = *it;
        if(p.extension().compare(path(".so")) == 0)
        {
            //std::string p.string
            const char* fname = p.string().c_str();
            void *handle = dlopen(fname,  RTLD_NOW | RTLD_GLOBAL);
            if (!handle)
            {
                std::cout << "Can't load library: " << p << std::endl;
                std::cout << "Reason: " << dlerror() << std::endl;
                continue;
            }

            //*(void **) (&cosine)
            PI_INIT InitPlugin;
            *(void **) (&InitPlugin) = (dlsym(handle, "InitPlugin"));
            if (!InitPlugin)
            {
                std::cout << "Can't load function \"InitPlugin\": " << p << std::endl;
                dlclose(handle);
                continue;
            }

            PluginInterface interface = (*InitPlugin) ();
            plugins.push_back(interface);

            SharedLibrary lib;
            lib.handle = handle;
            libs.push_back(lib);
            std::cout << "[SUCCESS] Plugin loaded: " << p << std::endl;
        }
        else 
        {
            std::cout << "Not a shared library: " << p << std::endl;
        }
    }
}

void PluginSystem::unloadAll()
{
    std::list<SharedLibrary>::const_iterator it(libs.begin());
    for(; it != libs.end(); ++it)
    {
        if(dlclose(it->handle))
        {
            std::cout << "Can't unload/close shared library" << std::endl;
        }
    }
    libs.clear();
    plugins.clear();
}

PluginSystem& PluginSystem::getInstance()
{
    static PluginSystem instance;
    return instance;
}

std::list<PluginInterface> PluginSystem::getPlugins()
{
    return plugins;
}


