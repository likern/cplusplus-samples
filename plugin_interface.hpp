#ifndef _PLUGIN_INTERFACE_HPP_
#define _PLUGIN_INTERFACE_HPP_

#include <boost/filesystem.hpp>

#ifdef __cplusplus
extern "C" {
#endif

enum PluginType {FTP, HTTP};

struct PluginInfo
{
    int minor;
    int major;
    PluginType type;
};

typedef PluginInfo (*PI_INFO) (void);
typedef void *(*PI_CREATE) (std::string &source);
typedef void (*PI_DESTROY) (void *);

struct PluginInterface
{
    PluginInfo info;
    PI_CREATE create;
    PI_DESTROY destroy;
};

typedef PluginInterface (*PI_INIT) (void);

class PluginObjectInterface
{
    public:
        //PluginObjectInterface(std::string &source);
        //virtual ~PluginObjectInterface();
        virtual void saveToFile(boost::filesystem::path &p) = 0;
};

/* Creates plugin object derived from DownloadInterface */
PluginInterface InitPlugin();
//PluginType pluginGetInfo();
//void *CreatePlugin(std::string &source);
//void DeletePlugin(void *);

#ifdef __cplusplus
}
#endif

#endif