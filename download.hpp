#ifndef _DOWNLOAD_HPP_
#define _DOWNLOAD_HPP_

#include <curl/curl.h>
#include <boost/filesystem.hpp>

class CurlEasy
{
    public:
        CurlEasy();
        ~CurlEasy();
    protected:
        CURL *handle;
    private:
        static int count;
};

#ifdef __cplusplus
extern "C" {
#endif

size_t writeToFile(char *ptr, size_t size, size_t nmemb, void *data);

#ifdef __cplusplus
}
#endif

#endif