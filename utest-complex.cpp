#include <gtest/gtest.h>
#include "complex.hpp"

TEST(ComplexTest, DefaultComplex)
{
    Complex res;
    EXPECT_EQ(0, res.getReal());
    EXPECT_EQ(0, res.getImag());
}

TEST(ComplexTest, AddTwoComplex)
{
    Complex one(2, 3);
    Complex two(4, 7);
    Complex res = one + two;
    EXPECT_EQ(6, res.getReal());
    EXPECT_EQ(10, res.getImag());
}

TEST(ComplexTest, MinusTwoComplex)
{
    Complex one(3, 15);
    Complex two(8, 6);
    Complex res = one - two;
    EXPECT_EQ(-5, res.getReal());
    EXPECT_EQ(9, res.getImag());
}

TEST(ComplexTest, MultiplyTwoComplex)
{
    Complex one(7, 9);
    Complex two(4, 2);
    Complex res = one * two;
    EXPECT_EQ(10, res.getReal());
    EXPECT_EQ(50, res.getImag());
}

TEST(ComplexTest, DivideTwoComplex)
{
    Complex one(7, 9);
    Complex two(4, 2);
    Complex res = one / two;
    EXPECT_EQ(2.3, res.getReal());
    EXPECT_EQ(1.1, res.getImag());
}

TEST(ComplexTest, DivideComplexByZero)
{
    Complex one(7, 9);
    Complex zero(0, 0);
    ASSERT_ANY_THROW(one / zero);
}

TEST(ComplexTest, MultiplyComplexByReal)
{
    Complex one(8, 19);
    Complex res = one * 3;
    EXPECT_EQ(24, res.getReal());
    EXPECT_EQ(57, res.getImag());
}

TEST(ComplexTest, AbsoluteComplex)
{
    Complex one(3, 4);
    double res = one.module();
    EXPECT_EQ(5, res);
}